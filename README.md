# backOffice

A back office is the portion of a company made up of administration and support personnel who are behind-the-scenes and not client-facing, while front office staffs are the folks in contact with the consumers or clients. Back office functions include